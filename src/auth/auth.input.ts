export class AuthLoginInput {
  email: string;
  password: string;
}