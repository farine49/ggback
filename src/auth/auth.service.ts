import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginResponse } from 'src/app/types/login-response.type';
import { UserDocument } from 'src/user/user.schema';

@Injectable()
export class AuthService {
  constructor (
    private readonly jwtService: JwtService) { }

  async Login(user: UserDocument): Promise<LoginResponse> {
    const payload = { id: user._id };
    const token = this.jwtService.sign(payload);

    return { token };
  }
}
