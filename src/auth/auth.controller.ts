import { BadRequestException, Body, Controller, Get, Post, UnauthorizedException, UseGuards } from '@nestjs/common';
import { LoginResponse } from 'src/app/types/login-response.type';
import { UserService } from 'src/user/user.service';
import { AuthLoginInput } from './auth.input';
import { AuthService } from './auth.service';
import * as BCrypt from "bcrypt";
import { UserDocument } from 'src/user/user.schema';
import { AuthUser } from 'src/app/decorators/auth-user.decorator';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor (
    private readonly authService: AuthService,
    private readonly userService: UserService) { }

  @Post("/login")
  async Login(@Body() authLoginInput: AuthLoginInput): Promise<LoginResponse> {
    const user = await this.userService.List({ email: authLoginInput.email });

    if (user.length === 0) {
      throw new BadRequestException("User not found");
    }

    const passwordMatch = BCrypt.compareSync(authLoginInput.password, user[0].password);

    if (!passwordMatch) {
      throw new UnauthorizedException("Password does not match");
    }

    return this.authService.Login(user[0]);
  }

  @Get("/me")
  @UseGuards(AuthGuard("jwt"))
  async Me(@AuthUser() user: any): Promise<UserDocument> {
    return user;
  }
}
