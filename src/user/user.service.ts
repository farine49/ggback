import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './user.schema';
import { Model } from "mongoose";
import { UserCreateInput, UserEditInput, UserListInput } from './user.input';
import * as BCrypt from "bcrypt";
import { ObjectId } from 'mongodb';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,) { }

  async List(userListInput: UserListInput): Promise<UserDocument[]> {
    return this.userModel.find(userListInput);
  }

  async Create(userCreateInput: UserCreateInput): Promise<UserDocument> {
    const user = new this.userModel(userCreateInput);
    user.password = await BCrypt.hash(user.password, 10);
    return user.save();
  }

  async Edit(userId: ObjectId, userEditInput: UserEditInput): Promise<UserDocument> {
    return this.userModel.findByIdAndUpdate(userId, userEditInput, { new: true });
  }
*
  async Delete(userId: ObjectId): Promise<UserDocument> {
    return this.userModel.findByIdAndDelete(userId);
  }

  
}
