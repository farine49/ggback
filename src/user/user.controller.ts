import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ObjectId } from 'mongodb';
import { AuthUser } from 'src/app/decorators/auth-user.decorator';
import { UserCreateInput, UserEditInput } from './user.input';
import { UserDocument } from './user.schema';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Get('/id/:id')
  async FindById(@Param('id') _id: string): Promise<UserDocument[]> {
    return this.userService.List({ _id: new ObjectId(_id) });
  }

  @Get("/email/:email")
  async FindByEmail(@Param("email") email: string): Promise<UserDocument[]> {
    return this.userService.List({ email });
  }

  

  @Post()
  async Create(@Body() userCreateInput: UserCreateInput): Promise<UserDocument> {
    return this.userService.Create(userCreateInput);
  }

  @Patch("/:id")
  async Edit(@Param("id") _id: string, @Body() userEditInput: UserEditInput): Promise<UserDocument> {
    return this.userService.Edit(new ObjectId(_id), userEditInput);
  }

  @Delete("/:id")
  async Delete(@Param("id") _id: string): Promise<UserDocument> {
    return this.userService.Delete(new ObjectId(_id));
  }
}
