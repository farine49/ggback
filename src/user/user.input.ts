import { ObjectId } from "mongodb";

export class UserListInput {
  _id?: ObjectId;
  email?: string;
}

export class UserCreateInput {
  email: string;
  password: string;
}

export class UserEditInput {
  email?: string;
  password?: string;
}
