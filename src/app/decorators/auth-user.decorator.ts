import { createParamDecorator } from '@nestjs/common';

/**
 * Use this when you need a connected user
 */
export const AuthUser = createParamDecorator((data, ctx) => {
  const req = ctx.switchToHttp().getRequest();
  return req.user;
});
