import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { ObjectId } from 'mongodb';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private configService : ConfigService,
    private userService: UserService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: configService.get('JWT_SECRET'),
    });
  }
  
  async validate(payload: any) {
    if (!payload.id) throw new Error('No user id found');
    if (!ObjectId.isValid(payload.id)) throw new Error('Invalid user id');
    
    const _id = ObjectId.createFromHexString(payload.id);

    const user = await this.userService.List({ _id });   

    if (user.length === 0) throw new Error('User not found');

    return user[0];
  }
}