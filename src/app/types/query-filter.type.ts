export type QueryFilter = {
  skip?: string;
  limit?: string;
}